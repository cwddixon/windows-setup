# Main setup script to:
# > Install programs
# > Set Windows features

# Install script for programs
$ProgramURI="https://gitlab.com/cwddixon/windows-setup/-/raw/main/programs"
$Programs=(Invoke-WebRequest -Uri $ProgramURI).Content
ForEach ($Program in $Programs -split "\n") {
    if ( $Program -match '^\s*#') { continue }
    if ( $Program -match '^\s*winget\s+') {
        $WingetArgs = $Program -replace '^\s*winget\s+',''
        winget install $WingetArgs
    }
}

# Set Windows features
iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/cwddixon/windows-setup/-/raw/main/winfeatures.ps1'))
Write-Host -ForegroundColor black -BackgroundColor green "`n`nRestart to complete`n"