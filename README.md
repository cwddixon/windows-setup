# 🪟 Windows Setup

Setup script for windows 10 and 11.

⭐ Current features:
* Install Programs from
  * Winget - Also provides install from winstore with `--source msstore` flag.
  * Install Windows components through dism

📌 Planned features:
* Install from custom places
* Update drivers
* Update windows
* Setup theme
* Activate Windows


# 💿 Install script

```powershell
iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/cwddixon/windows-setup/-/raw/main/start.ps1'))
```

# 🗺️ Project layout

## 📃 Programs

Space seperated list with install method and the program to install. Commented lines with # are ignored.

| Install method name | Descirption |
| --- | --- |
| # | Comment line and is ignored |
| [winget](https://learn.microsoft.com/en-us/windows/package-manager/winget/#commands) | Calls winget and passes the rest of the line to the command. See windows docs for flags. If there is more then one source use `--source msstore`. |

## ➕➖ Windows features

Powershell file that contains [dism](https://learn.microsoft.com/en-us/windows-hardware/manufacture/desktop/what-is-dism?view=windows-11) commands to enable and disable windows features. This won't reset the PC but it will need to be restarted for it to take affect.